package io.finer.ads.jeecg.mapper;

import io.finer.ads.jeecg.entity.Ad;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 广告
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface AdMapper extends BaseMapper<Ad> {

}
