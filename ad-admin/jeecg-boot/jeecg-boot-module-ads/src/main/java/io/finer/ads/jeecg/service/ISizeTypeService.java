package io.finer.ads.jeecg.service;

import io.finer.ads.jeecg.entity.SizeType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 广告常规尺寸
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface ISizeTypeService extends IService<SizeType> {

}
