const Config = require('../config/index');

let adCode = {
    method: 'GET',
    path: '/js/{param*}',
    handler: {
        directory: {
            path: 'public/js',
            listing: false
        }    
        
    }
};

let adImage = {
    method: 'GET',
    path: '/images/{param*}',
    handler: {
        directory: {
            path: 'public/images',
            listing: false
        }    
    }
};

let adFile = {
    method: 'GET',
    path: '/adfile/{param*}',
    handler: {
        directory: {
            path: Config.path_upload_adfile,
            listing: false
        }    
    }
};

let adTest = {
    method: 'GET',
    path: '/demo/{param*}',
    handler: {
        directory: {
            path: 'demo',
            listing: false
        }    
    }
};

module.exports = [adCode, adImage, adFile, adTest];
